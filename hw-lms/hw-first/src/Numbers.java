import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static int checkOnError(Scanner sc) {
        while (true) {
            System.out.print("What is the number: ");
            if (!sc.hasNextInt()) {
                System.out.println("Please enter a valid number.");
                sc.next();
            }
            else{
                return sc.nextInt();
            }
        }
    }

    public static void main(String[] args) {
        Random random = new Random();
        int name = random.nextInt(101);
        int [] numbers = new int [0];
        System.out.println("Let the game begin!");
        Scanner sc = new Scanner(System.in);
        while (true) {
            int userGuess = checkOnError(sc);
            int [] newNumbers = new int [numbers.length + 1];
            for(int i = 0; i < numbers.length; i++){
                newNumbers[i] = numbers[i];
            }
            newNumbers[numbers.length] = userGuess;
            numbers = newNumbers;
            if (userGuess == name) {
                System.out.printf("Congratulations, %d!\n", name);
                System.out.print("Your numbers: ");
                int checkOfEnd = 0;
                while(checkOfEnd < numbers.length - 1){
                    if(numbers[checkOfEnd] < numbers[checkOfEnd + 1]){
                        int replace = numbers[checkOfEnd + 1];
                        numbers[checkOfEnd + 1] = numbers[checkOfEnd];
                        numbers[checkOfEnd] = replace;
                        checkOfEnd = 0;
                    }
                    checkOfEnd++;
                }
                for(int i = 0; i < numbers.length; i++){
                    System.out.print(numbers[i] + ", ");
                }
                break;
            } else if (userGuess > name) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Your number is too small. Please, try again.");
            }
        }
        sc.close();
    }
}
