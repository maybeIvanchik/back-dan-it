import java.util.Random;
import java.util.Scanner;

public class AreaShooting {

    public static char checkOfShoot(int[] userPos, int[] secretPos) {
        for (int i = 0; i < 2; i++) {
            if (userPos[i] != secretPos[i]) {
                return '*';
            }
        }
        return 'x';
    }

    public static int checkOnError(String coordinateName, Scanner sc) {
        while (true) {
            System.out.print("Print " + coordinateName + " pos (1-5): ");
            if (sc.hasNextInt()) {
                int pos = sc.nextInt();
                if (pos >= 1 && pos <= 5) {
                    return pos - 1;
                } else {
                    System.out.println("Please enter a number between 1 and 5.");
                }
            } else {
                System.out.println("Please enter a valid number.");
                sc.next();
            }
        }
    }

    public static void generateBoard(final int SIZE, char [][] board){
        for(int i = 0; i < SIZE; i++){
            for(int j = 0; j < SIZE; j++){
                board[i][j] = '-';
            }
        }
    }

    public static void createBoard(final int SIZE, char[][] board) {
        for (int i = 0; i <= SIZE; i++) {
            System.out.print(i + " | ");
        }
        System.out.println();
        for (int i = 0; i < SIZE; i++) {
            System.out.print((i + 1) + " |");
            for (int j = 0; j < SIZE; j++) {
                System.out.print(" " + board[i][j] + " |");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        final int SIZE = 5;
        int[] secretPos = new int[2];
        int[] userPos = new int[2];
        char[][] board = new char[SIZE][SIZE];
        Random random = new Random();
        for (int i = 0; i < 2; i++) {
            secretPos[i] = random.nextInt(5);
        }
        Scanner sc = new Scanner(System.in);
        System.out.println("All Set. Get ready to rumble!");
        generateBoard(SIZE, board);
        while (true) {
            createBoard(SIZE, board);
            userPos[0] = checkOnError("x", sc);
            userPos[1] = checkOnError("y", sc);
            char Symbol = checkOfShoot(userPos, secretPos);
            if(Symbol == 'x'){
                board[userPos[0]][userPos[1]] = Symbol;
                createBoard(SIZE, board);
                System.out.println("You won!!");
                break;
            }
            else{
                board[userPos[0]][userPos[1]] = Symbol;
            }
        }
        sc.close();
    }
}
